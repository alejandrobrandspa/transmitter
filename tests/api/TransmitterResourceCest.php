<?php
use \ApiTester;

class TransmitterResourceCest
{
	protected $endpoint = "/api/transmitter";
	protected $data;

    public function _before(ApiTester $I)
    {
    	$this->data = [
	    	'name' => '',
	    	'year_of_incoporation' => '',
	    	'web_page' => '',
	    	'sector' => '',
	    	'city' => '',
	    	'employees' => '',
	    	'description_es' => '',
	    	'description_en' => '',
	    	'market_capitalization' => '',
	    	'market_capitalization_closure_date' => '',
	    	'number_of_shareholders' => '',
	    	'total_shares_outstanding' => '',
	    	'free_float' => '',
	    	'dividend' => '',
	    	'earnings_per_share' => '',
	    	'note' => '',
	    	'badge' => '',
	    	'ticker_1' => '',
	    	'ticker_2' => '',
	    	'type_of_stock_1' => '',
	    	'type_of_stock_2' => '',
	    	'isin_1' => '',
	    	'isin_2' => '',
	    	'instrument_type_1' => '',
	    	'instrument_type_2' => '',
	    	'price_book_1' => '',
	    	'price_book_2' => '',
	    	'p_e_1' => '',
	    	'p_e_2' => '',
	    	'dividend_yield_1' => '',
	    	'dividend_yield_2' => '',
	    	'shares_outstanding_1' => '',
	    	'shares_outstanding_2' => '',
	    	'responsability_policy_es' => '',
	    	'responsability_policy_en' => '',
	    	'financial_figures_graph' => '',
	    	'main_shareholders_graph' => '',
	    	'stock_performance_graph' => '',
	    	'revenue_segmentation_graph' => ''
    	];

    }

    public function _after(ApiTester $I)
    {
    	DB::table('transmitters')->truncate();
    }


}