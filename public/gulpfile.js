var gulp = require('gulp');

// Include our plugins
var browserify = require('browserify');
var del = require('del'); // Delete files/folders using globs
var hbsfy = require("hbsfy"); // compile templates
var source = require('vinyl-source-stream'); // Use conventional text streams at the start of your gulp
var sourcemaps = require('gulp-sourcemaps'); // Write inline source maps
var watch = require('gulp-watch');

// compile browersify app admin
gulp.task('compile-admin', function () {
  var options;

  options = {
    debug: true,
    paths: [
    './node_modules',
    './js/app/admin'
    ]
  };

  hbsfy.configure({
    extensions: ['hbs']
  });

  browserify('./js/app/admin/admin.js', options)
  .transform(hbsfy)
  .bundle()
  .pipe(source('app_admin.js'))
  .pipe(gulp.dest('js/dist'));
});

gulp.task('watch', ['compile-admin'], function(){
  gulp.watch([
    'js/app/admin/**/*.js', 
    'js/app/admin/**/**/*.js', 
    'js/app/admin/**/*.hbs', 
    'js/app/admin/**/**/*.hbs', 
    'js/app/**/*.js'
    ], 
    [
      'compile-admin'
    ]);
});

gulp.task('default', ['watch']);
