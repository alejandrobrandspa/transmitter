var pubsub = require('./pubsub');

module.exports = {
  
  initialize: function() {
    var _this = this;
    pubsub.on('local:fetch', _this.fetch, _this);
    pubsub.on('local:store', _this.store, _this);
    pubsub.on('local:delete', _this.delete, _this);
  },

  fetch: function(key) {
    return localStorage.getItem(key);
  },

  store: function(data) {
    var key = data[0];
    var val = data[1];
    return localStorage.setItem(key, val);
  },

  delete: function(key) {
    return localStorage.removeItem(key);
  },

  flush: function() {

  }
};