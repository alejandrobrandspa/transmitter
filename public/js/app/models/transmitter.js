tr.models.Transmitter = Backbone.Model.extend({
  urlRoot: '/transmitter'
});

tr.collections.Transmitters = Backbone.Collection.extend({
  url: '/transmitter',
  model: tr.models.Transmitter
});

