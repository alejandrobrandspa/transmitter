var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/transmitter');

module.exports = Backbone.Collection.extend({
  url: "/api/transmitter",
  model: model
});

