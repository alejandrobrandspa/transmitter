var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/strength');

module.exports = Backbone.Collection.extend({
  url: "/api/strengths",
  model: model
});
