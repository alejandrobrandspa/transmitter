var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/executive');

module.exports = Backbone.Collection.extend({
  url: "/api/executives",
  model: model
});