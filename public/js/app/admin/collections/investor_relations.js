var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/investor_relation');

module.exports = Backbone.Collection.extend({
  url: '/api/investorrelations',
  model: model
});