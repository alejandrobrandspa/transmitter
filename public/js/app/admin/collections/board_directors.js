var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/board_director');

module.exports = Backbone.Collection.extend({
  url: "/api/boarddirectors",
  model: model
});