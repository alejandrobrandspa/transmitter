var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var Router = require('./routes');

var router = new Router();
function nextTab(tabNum){
  $('#inter-nav li:eq('+ tabNum +') a').tab('show');
  return false;
}

Backbone.history.start();