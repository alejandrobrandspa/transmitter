//Dependencies
var Backbone = require('backbone');
var $ = require('jquery');

//Controllers
var TransmitterCreateController = require('./controllers/transmitter_create');
var TransmitterEditController = require('./controllers/transmitter_edit');
var TransmitterListController = require('./controllers/transmitter_list');

//Utils
var pubsub = require('../utils/pubsub');

Backbone.$ = $;

module.exports = Backbone.Router.extend({

  routes: {
    '': "transmittersList",
    'transmitters/create': "transmitterCreate",
    'admin/transmitters/:id/edit': "transmitterEdit"
  },

  initialize: function(){
    var _this = this;
    var currentViews;
    _this.currentViews = [];

    //Listen events on controller
    TransmitterCreateController.initialize();

    //Listen for remove views
    pubsub.on('views:remove', _this.cleanCurrentViews, _this);
  },

  /**
   * execute appropriate method when the url match
   * @param  {Function} callback all the function
   */
  execute: function(callback, args){
    pubsub.trigger('view:remove');
    if (callback) callback.apply(this, args);
  },

  transmittersList: function(query) {
    var _this = this;
    TransmitterListController.initialize();
    var page = TransmitterListController.page();
    var list = TransmitterListController.getList();
    var listFilters = TransmitterListController.listFilters();
  },

  transmitterCreate: function() {
    var _this = this;

    TransmitterCreateController.page();

    TransmitterCreateController.create();

    TransmitterCreateController.graphCreate();
    
    TransmitterCreateController.boardDirectorCreate();
    
    TransmitterCreateController.strengthCreate();
    
    TransmitterCreateController.executiveCreate();
    
    TransmitterCreateController.investorRelationCreate();
  },

  transmitterEdit: function(id) {
    var _this = this;

    var editApp  = TransmitterEditController.initialize(id);

    var page = TransmitterEditController.page();

    var edit = TransmitterEditController.edit(id);
   
    var graphs = TransmitterEditController.graphs(id);
   
    var boardDirectors = TransmitterEditController.boardDirectors(id);
    
    var strengths = TransmitterEditController.strengths(id);

    var executives = TransmitterEditController.executives(id);
    
    var investorRelations = TransmitterEditController.investorRelations(id);
  }

});
