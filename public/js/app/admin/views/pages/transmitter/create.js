//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
//Utils
var pubsub = require('../../../../utils/pubsub');
//Templates
var template = require('../../../templates/pages/transmitter/create.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  initialize: function(){
    pubsub.on('view:remove', this.remove, this);
  },

  render: function(){
    this.$el.html(template());
    return this;
  }
});