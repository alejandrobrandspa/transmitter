//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/executives/form.hbs');

//relation backbone and jquery
Backbone.$ = $;

module.exports = Backbone.View.extend({
  template: "#executive-form-template", 

  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    this.$el.html(template());
    return this;
  },

  /**
   * Get data from form 
   * get transmitter id from localStorage
   * send data to api
   * @param  {[type]} e [description]
   * @return ajax
   */
  save: function(e){
    var _this = this;
    var $el = $(e.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return this.model.save(data).done(_this.responseStore);
  },

  /**
   * notify was store model
   * @return event pusbsub
   */
  responseStore: function() {
    toastr.success('Guardado');

    if (this.type == "POST") {
      return pubsub.trigger('executive:render');
    }
  },

  store: function(e) {
    e.preventDefault();
    this.save(e);
  }
});