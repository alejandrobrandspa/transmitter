//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/board_directors/form.hbs'); 

//relation backbone and jquery
Backbone.$ = $;

module.exports =  Backbone.View.extend({

  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * append template to $el with the model data
   * @return object this
   */
  render: function(){
    var _this = this;
    _this.$el.html( template( _this.model.toJSON() ) );
    return _this;
  },

  /**
   * get data from form 
   * send data to server
   * and send message to user
   * @param  jquery event evt
   * @return ajax
   */
  save: function(evt){
    var _this = this;
    var _evt = evt;
    var $el = $(_evt.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return _this.model.save(data).done(_this.saveResponse);
  },

  /**
   * ajax response 
   * @return {[type]} [description]
   */
  saveResponse: function() {
    toastr.success('Guardado');
  },
  
  /**
   * execute save method
   * @param  jquery event evt
   * @return function 
   */
  store: function(evt) {
    evt.preventDefault();
    return this.save(evt);
  },


});