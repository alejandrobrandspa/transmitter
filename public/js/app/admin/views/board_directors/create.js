//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/board_directors/form.hbs'); 

//relation backbone and jque
Backbone.$ = $;

module.exports = Backbone.View.extend({

  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * append tamplate to el
   * @return object this
   */
  render: function(){
    this.$el.html(template());
    return this;
  },

  /**
   * get data from form 
   * send data to server
   * and send message to user
   * @param  jquery event evt
   * @return ajax
   */
  save: function(evt){
    var _this = this;
    var _evt = evt;
    var $el = $(_evt.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return _this.model.save(data).done(_this.responseStore);

  },
  
  responseStore: function() {
    toastr.success('Guardado');

    if (this.type == "POST") {
      pubsub.trigger('boardDirector:render');
    }
  },
  
  /**
   * execute save method
   * @param  jquery event evt
   * @return function 
   */
  store: function(evt) {
    evt.preventDefault();
    return this.save(evt);
  }

});