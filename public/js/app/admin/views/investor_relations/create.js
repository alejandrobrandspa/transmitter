//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/investor_relations/form.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    this.$el.html(template());
    return this;
  },

  save: function(evt){
    var _this = this;
    var $el = $(evt.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    _this.model.save(data).done(_this.responseStore);
  },
  
  /**
   * notify was store model
   * @return event pusbsub
   */
  responseStore: function() {
    toastr.success('Guardado');

    if (this.type == "POST") {
      return pubsub.trigger('investorRelation:render');
    }
  },

  store: function(evt) {
    var _evt = evt;
    _evt.preventDefault();

    this.save(_evt);

    toastr.success('Guardado');
  }
});