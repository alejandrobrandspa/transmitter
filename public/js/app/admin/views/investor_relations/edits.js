//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Views
var editView = require('./edit');

//Utils
var pubsub = require('../../../utils/pubsub');

//list all the forms
module.exports = Backbone.View.extend({

  /**
   * Initialize view and listen events
   */
  initialize: function(){
    var _this = this;

    _this.listenTo(_this.collection, 'reset', _this.render);
    _this.listenTo(_this.collection, 'reset', _this.renderForm);
  },

  /**
   * append all the forms with data for edit
   * @return object el
   */
  render: function(){
    var _this = this;
    var html = [];
    var view;
  
    _this.collection.each(function(model){
      view = new editView( {model: model} );
      html.push(view.render().el);
    }, _this);

    _this.$el.html(html);
    return _this;
  },

  /**
   * Execute event to append form 
   */
  renderForm: function() {
    return pubsub.trigger('investorRelation:render');
  }
});