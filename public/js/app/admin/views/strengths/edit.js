//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/strengths/form.hbs'); 

Backbone.$ = $;

module.exports = Backbone.View.extend({
  
  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save",
  },

  render: function(){
    var _this = this;
    $(_this.el).html(template( _this.model.toJSON() ));
    return _this;
  },
  
  /**
   * get the data and send to server
   * @param  {[type]} e [description]
   * @return {[type]}   [description]
   */
  save: function(e){
    var _this = this;
    var $el = $(e.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return _this.model.save(data).done(_this.saveResponse);
  },

  /**
   * notify stored
   * @return {[type]} [description]
   */
  saveResponse: function() {
    toastr.success('Guardado');
    if (this.type == "POST") {
      _this.stored();
    }
  },

  /**
   * Execute method store
   * @param  jquery event
   * @return function
   */
  store: function(e) {
    e.preventDefault();
    return this.save(e);
  },

  /**
   * trigger event
   */
  stored: function(){
   return pubsub.trigger('strength:render');
  }
});