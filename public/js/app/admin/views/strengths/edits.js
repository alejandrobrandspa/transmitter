//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Views
var editView = require('./edit');

Backbone.$ = $;

module.exports = Backbone.View.extend({

  /**
   * listen events
   */
  initialize: function(){
    this.listenTo(this.collection, 'reset', this.render);
    this.listenTo(this.collection, 'reset', this.renderForm);
  },
  
  /**
   * append all the forms with data for edit
   * @return object el
   */
  render: function(){
    var _this = this;
    var html = [];
    var view;
    
    _this.collection.each( function(model) {
      view = new editView({model: model});
      html.push(view.render().el);
    }, _this);

    _this.$el.html(html);
    return _this;
  },

  /**
   * Execute event to append form 
   */
  renderForm: function() {
    return pubsub.trigger('strength:render');
  }
});