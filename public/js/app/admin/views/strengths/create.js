//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/strengths/form.hbs'); 

Backbone.$ = $;

module.exports = Backbone.View.extend({
  
  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save",
  },

  render: function(){
    $(this.el).html(template());
    return this;
  },

  save: function(e){
    var _this = this;
    var $el = $(e.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return _this.model.save(data).done(function(){
      toastr.success('Guardado');
      if (this.type == "POST") {
        pubsub.trigger('strength:render');
      }
    });
  },

  store: function(e) {
    e.preventDefault();
    this.save(e);
  },

});