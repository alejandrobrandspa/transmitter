//Dependencies
var $ = require('jquery');
var Backbone = require('backbone');

//Utils
var pubsub = require('../../../utils/pubsub');
var template = require('../../templates/transmitters/list_filters.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({

  events: {
    'change .query': 'filterByQuery',
    'change .sector': 'filterBySector'
  },

  initialize: function() {
    var _this = this;
    _this.filters = {};
  },

  /**
   * append template to el
   * @return this
   */
  render: function() {
    $(this.el).html(template);
    return this;
  },

  /**
   * Send filters by pubsub
   * @return {[type]} [description]
   */
  filter: function(filters) {
    this.filters = _.extend(this.filters, filters);
    pubsub.trigger('transmitters:filter', this.filters);
  },

  /**
   * filter by query
   * @return event jquery
   */
  filterByQuery: function(e) {
    var $el = $(e.currentTarget);
    var data = {'query': $el.val() };
    this.filter(data);
  },

   /**
   * filter by sector
   * @return event jquery
   */
  filterBySector: function(e) {
    var $el = $(e.currentTarget);
    var data = {'sector': $el.val() };
    this.filter(data);
  }
});