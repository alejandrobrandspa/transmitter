//Dependencies
var $ = require('jquery');
var Backbone = require('backbone');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/transmitters/form_graphs.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  events: {
    "change .file": "store"
  },

  initialize: function(){
    var _this = this;
    _this.$file = '';

     //listen changes when model is updated or fetch.
    _this.listenTo(_this.model, 'change', _this.render);
    //attach object this to methods
    _.bindAll(_this, 'showImageAdded');
    //remove this view on event
    pubsub.on('view:remove', _this.remove, _this);
  },
  
  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    var _this = this;
    _this.$el.html(template( _this.model.toJSON() ));
    return _this;
  },

  /**
   * get file and send to server
   * @param  jquery event evt
   * @return ajax
   */
  store: function(evt){
    var _evt = evt;
    var _this = this;
    var id = localStorage.transmitter_id;
    var formData;
    var name;
    var file;

    _evt.preventDefault();

    _this.$file = $(_evt.currentTarget);
    name = _this.$file.attr('name');
    file = _this.$file[0].files[0];

    formData = new FormData();
    formData.append(name, file);

    //ajax options
    var options = {
      url: '/api/transmitter/' + id + '/graph',
      type: 'POST',
      data: formData,
      processData: false, //Avoid be processed by jquery
      contentType: false, //Not set any content type header
    };

    return $.ajax(options).done(_this.showImageAdded);
  },

  /**
   * search img tag and attach image url
   * @param  object res
   */
  showImageAdded: function(res){
    var _this = this;
    var template = "<img src='/uploads/"+ res +"' alt='' class='img-responsive'>";
    _this.$file.parent().find('.img-container').html(template);
  }

});