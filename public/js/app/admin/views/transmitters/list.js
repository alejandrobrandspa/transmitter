//Dependencies
var $ = require('jquery');
var Backbone = require('backbone');

//Models
var transmitterItem = require('./item');

//Utils
var pubsub = require('../../../utils/pubsub');

//backbone relation with jquery
Backbone.$ = $;

module.exports = Backbone.View.extend({
  tagName: "tbody",
  
  initialize: function(){
    var _this = this;
    //listen reset event for render
    _this.listenTo(_this.collection, 'reset', _this.render);
    //remove this view on event
    pubsub.on('view:remove', _this.remove, _this);
    pubsub.on('transmitters:filter', _this.filter, _this);

  },

  /**
   * For each model inside collection
   * render new item 
   * append to html array
   * append html to $el
   * @return {[type]} [description]
   */
  render: function(){
    var _this = this;
    var html = [];
    var view;

    _this.collection.each(function(model){
      view = new transmitterItem({model: model});
      html.push(view.render().el);
    }, _this);
    _this.$el.html(html);
  },

  filter: function(filters) {
    this.collection.fetch({reset: true, data: filters});
  }
});