//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/transmitters/item.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  tagName: "tr",

  events: {
    'click .edit': 'edit'
  },

  initialize: function(){
    //remove this view on event
    pubsub.on('view:remove', this.remove, this);
  },

  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    var _this = this;

    _this.$el.html(template(_this.model.toJSON()));
    return _this;
  },

  /**
   * trigger event to edit
   */
  edit: function(evt){
    evt.preventDefault();
    pubsub.trigger('transmitter:edit');
  }
  
});