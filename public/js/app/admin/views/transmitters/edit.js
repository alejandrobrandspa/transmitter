//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/transmitters/form.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  events: {
    "change .form-control": "store",
    "click input[type='checkbox']": "save",
    "click .next": "next",
  },

  initialize: function(){
    var _this = this;

    //listen changes when model is updated or fetch.
    _this.listenTo(_this.model, 'change', _this.render);
    //Bind methods to object
    _.bindAll(_this, 'stored');
    //remove this view on event
    pubsub.on('view:remove', _this.remove, _this);
  },
  
  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    var _this = this;
    _this.$el.html(template(_this.model.toJSON()));
  
    return _this;
  },

   /**
   * get data from this.el form and send it
   * @param  event
   * @return backbone ajax
   */
  save: function() {
    var _this = this;
    var $form = _this.$el.find('form');
    var data = $form.serializeJSON();
    return _this.model.save(data).done(_this.stored);
  },

  /**
   * prevent event and execute save
   * @param  event
   * @return function
   */
  store: function(evt) {
    evt.preventDefault();
    this.save();
  },

  /**
   * get the response from store
   * send succes message to user
   * save transmitter id on localStorage
   * and show tabs
   * @param  res [response from method store]
   */
  stored: function(res){
    var _this = this;
    
    toastr.success('Guardado');

    localStorage.transmitter_id = this.model.id;

    $('#inter-nav li').removeClass('hidden');
    $('.next').removeClass('hidden');
  }
});

