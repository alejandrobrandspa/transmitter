//Collections
var Transmitters = require('../collections/transmitters');

//Views
var PageTransmitterList = require('../views/pages/transmitter/list');
var TransmitterList = require('../views/transmitters/list');
var TransmitterListFilters = require('../views/transmitters/list_filters');

//Utils
var pubsub = require('../../utils/pubsub');
var local_storage = require('../../utils/local_storage');

//Controller
module.exports = {

  initialize: function(){
    local_storage.initialize();
    pubsub.trigger('local:delete', 'transmitter_id');
  },

  /**
   * Render page to body container
   * @return object view
   */
  page: function(){
    var view = new PageTransmitterList();
    $('body > .container').append(view.render().el);
    return view;
  },

  /**
   * Fetch data to server and append 
   * view with all collection to DOM
   * @return object jquery
   */
  getList: function(){
    var collection = new Transmitters();
    var view = new TransmitterList({collection: collection});

    collection.fetch({reset: true});

    return $('.transmitters-list').append(view.el);
  },

  listFilters: function() {
    var view = new TransmitterListFilters();
    return $('.transmitters-list-filters').append(view.render().el);
  }
};