//Models
var Transmitter = require('../models/transmitter');

//Collections
var BoardDirectors = require('../collections/board_directors');
var Strengths = require('../collections/strengths');
var Executives = require('../collections/executives');
var InvestorRelations = require('../collections/investor_relations');

//Views
var PageTransmitterEdit = require('../views/pages/transmitter/edit');
var TransmitterEdit = require('../views/transmitters/edit');
var GraphEdit = require('../views/transmitters/graph_edit');
var BoardDirectorsEdit = require('../views/board_directors/edits');
var StrengthsEdit = require('../views/strengths/edits');
var ExecutivesEdit = require('../views/executives/edits');
var InvestorRelationsEdit = require('../views/investor_relations/edits');

//Utils
var pubsub = require('../../utils/pubsub');
var local_storage = require('../../utils/local_storage');

//Controller
module.exports = {

  initialize: function(id) {
   
    local_storage.initialize();
    pubsub.trigger('local:store', ['transmitter_id', id]);
  },

  page: function() {
    var view = new PageTransmitterEdit();
    $('body > .container').append(view.render().el);
    return view;
  },

  /**
   * fetch model and render view 
   * to append his container
   * @return object view
   */
  edit: function(id) {
    var model = new Transmitter({id: id});
    var view = new TransmitterEdit( {model: model} );
    model.fetch();
    $('.transmitter-container').empty().append( view.render().el );
    return view;
  },

  /**
   * fetch model and render view 
   * to append his container
   * @return object view
   */
  graphs: function(id) {
    var model = new Transmitter({id: id});
    var view = new GraphEdit( {model: model} );
    model.fetch();
    $('.transmitter-graphs_container').empty().append( view.render().el );
    return view;
  },

  /**
   * append forms with model data
   * and fetch data from server
   * @param int id transmitter_id
   */
  boardDirectors: function(id) {
    var collection = new BoardDirectors();
    var view = new BoardDirectorsEdit({collection: collection});
    collection.fetch({ reset: true, data: { transmitter_id: id } });
    $('.board-director-container').append( view.render().el );
  },

  /**
   * append forms with model data
   * and fetch data from server
   * @param int id transmitter_id
   */
  strengths: function(id) {
    var collection = new Strengths();
    var view = new StrengthsEdit({collection: collection});
    collection.fetch({ reset: true, data: { transmitter_id: id } });
    $('.strength-container').append( view.render().el );
  },

  /**
   * append forms with model data
   * and fetch data from server
   * @param int id transmitter_id
   */
  executives: function(id) {
    var collection = new Executives();
    var view = new ExecutivesEdit( {collection: collection} );
    collection.fetch({ reset: true, data: { transmitter_id: id } });
    $('.executive-container').append( view.render().el );
  },

   /**
   * append forms with model data
   * and fetch data from server
   * @param int id transmitter_id
   */
  investorRelations: function(id) {
    var collection = new InvestorRelations();
    var view = new InvestorRelationsEdit( {collection: collection} );
    collection.fetch({ reset: true, data: { transmitter_id: id } });
    console.log($('.investor-relation-container').html());
    $('.investor-relation-container').append( view.render().el );
  }
};