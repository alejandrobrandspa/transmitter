//Models
var Transmitter = require('../models/transmitter');
var BoardDirector = require('../models/board_director');
var Strength = require('../models/strength');
var Executive = require('../models/executive');
var InvestorRelation = require('../models/investor_relation');

//Views
var PageTransmitterCreate = require('../views/pages/transmitter/create');
var TransmitterCreate = require('../views/transmitters/create');
var TransmitterGraphCreate = require('../views/transmitters/graph_create');
var BoardDirectorCreate = require('../views/board_directors/create');
var StrengthCreate = require('../views/strengths/create');
var ExecutiveCreate = require('../views/executives/create');
var InvestorRelationCreate = require('../views/investor_relations/create');

//Utils
var pubsub = require('../../utils/pubsub');
var local_storage = require('../../utils/local_storage');

//Controller
module.exports = {

  //listen to events from pubsub
  initialize: function(){
    var _this = this;
    pubsub.on('boardDirector:render', _this.boardDirectorCreate, _this);
    pubsub.on('strength:render', _this.strengthCreate, _this);
    pubsub.on('executive:render', _this.executiveCreate, _this);
    pubsub.on('investorRelation:render', _this.investorRelationCreate, _this);
    local_storage.initialize();
  },

  /**
   * Render page to body container
   * @return object view
   */
  page: function(){
    var view = new PageTransmitterCreate();
    $('body > .container').append(view.render().el);
    return view;
  },

  /**
   * Initialize view and append to a container
   * @return object view
   */
  create: function(){
    var model = new Transmitter();
    var view = new TransmitterCreate({ model: model });
    $('.transmitter-container').empty().append( view.render().el );
    return view;
  },

  /**
   * Initialize view and append to a container
   * @return object view
   */
  graphCreate: function(){
    var model = new Transmitter();
    var view =  new TransmitterGraphCreate({ model: model });
    $('.transmitter-graphs_container').empty().append( view.render().el );
    return view;
  },

  /**
   * Initialize view and append to a container
   * @return object view
   */
  boardDirectorCreate: function(){
    var model = new BoardDirector();
    var view = new BoardDirectorCreate({ model: model });
    $('.board-director-container').append( view.render().el );
    return view;
  },

  /**
  * Initialize view and append to a container
  * @return object view
  */
  strengthCreate: function(){
    var model = new Strength();
    var view = new StrengthCreate({ model: model });
    $('.strength-container').append( view.render().el );
    return view;
  },

  /**
  * Initialize view and append to a container
  * @return object view
  */
  executiveCreate: function(){
    var model = new Executive();
    var view = new ExecutiveCreate({ model: model });
    $('.executive-container').append( view.render().el  );
    return view;
  },

   /**
   * Initialize view and append to a container
   * @return object view
   */
  investorRelationCreate: function(){
    var model = new InvestorRelation();
    var view = new InvestorRelationCreate({ model: model });
    $('.investor-relation-container').append( view.render().el  );
    return view;  
  }
};