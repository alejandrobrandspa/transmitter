var $ = require('jquery');
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/transmitter"
});


var $ = require('jquery');
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/boarddirectors"
});


var $ = require('jquery');
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/strengths"
});

var $ = require('jquery');
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/executives"
});
var $ = require('jquery');
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: '/api/investorrelations'
});
var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/transmitter');

module.exports = Backbone.Collection.extend({
  url: "/api/transmitter",
  model: model
});


var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/strength');

module.exports = Backbone.Collection.extend({
  url: "/api/strengths",
  model: tr.admin.models.Strength
});

var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/executive');

module.exports = Backbone.Model.extend({
  url: "/api/executives",
  model: tr.admin.models.Executive
});
var $ = require('jquery')(window);
var Backbone = require('backbone');
var model = require('../models/investor_relation');

module.exports = Backbone.Collection.extend({
  url: '/api/investorrelations',
  model: tr.admin.models.InvestorRelation
});
//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/transmitters/form.hbs');
Backbone.$ = $;

module.exports = Backbone.View.extend({
  events: {
    "change .form-control": "store",
    "click .next": "next",
  },

  initialize: function(){
    var _this = this;

    //Bind methods to object
    _.bindAll(_this, 'stored');
  },
  
  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    var _this = this;
    _this.$el.html(template());
    return _this;
  },

  /**
   * get data from this.el form and send it
   * @param  event
   * @return backbone ajax
   */
  store: function(evt) {
    var _this = this;
    var $form = _this.$el.find('form');
    var data = $form.serializeJSON();

    evt.preventDefault();

    return _this.model.save(data).done(_this.stored);
  },

  /**
   * get the response from store
   * send succes message to user
   * save transmitter id on localStorage
   * and show tabs
   * @param  res [response from method store]
   */
  stored: function(res){
    var _this = this;
    
    toastr.success('Guardado');

    localStorage.transmitter_id = this.model.id;

    $('#inter-nav li').removeClass('hidden');
    $('.next').removeClass('hidden');
  }
});
//Dependencies
var $ = require('jquery');
var Backbone = require('backbone');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/transmitters/form_graphs.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  events: {
    "change .file": "store"
  },

  initialize: function(){
    var _this = this;
    _this.$file = '';

    //attach object this to methods
    _.bindAll(_this, 'showImageAdded');
    
  },
  
  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    var _this = this;
    _this.$el.html(template());
    return _this;
  },

  /**
   * get file and send to server
   * @param  jquery event evt
   * @return ajax
   */
  store: function(evt){
    var _evt = evt;
    var _this = this;
    var id = localStorage.transmitter_id;
    var formData;
    var name;
    var file;

    _evt.preventDefault();

    _this.$file = $(_evt.currentTarget);
    name = _this.$file.attr('name');
    file = _this.$file[0].files[0];

    formData = new FormData();
    formData.append(name, file);

    //ajax options
    var options = {
      url: '/api/transmitter/' + id + '/graph',
      type: 'POST',
      data: formData,
      processData: false, //Avoid be processed by jquery
      contentType: false, //Not set any content type header
    };

    return $.ajax(options).done(_this.showImageAdded);
  },

  /**
   * search img tag and attach image url
   * @param  object res
   */
  showImageAdded: function(res){
    var _this = this;
    _this.$file.parent().find('img').attr('src', '/uploads/' + res);
  }


});
//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/board_directors/form.hbs'); 

Backbone.$ = $;

module.exports =  Backbone.View.extend({
  template: "#board-director-form-template", 

  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * get template
   * compile the template
   * and append to $el
   * @return object this
   */
  render: function(){
    this.$el.html(template());
    return this;
  },

  /**
   * get data from form 
   * send data to server
   * and send message to user
   * @param  jquery event evt
   * @return ajax
   */
  save: function(evt){
    var _this = this;
    var _evt = evt;
    var $el = $(_evt.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return _this.model.save(data).done(function(){
      toastr.success('Guardado');

      if (this.type == "POST") {
        _this.stored();
      }
    });

  },
  
  /**
   * execute save method
   * @param  jquery event evt
   * @return function 
   */
  store: function(evt) {
    evt.preventDefault();
    return this.save(evt);
  },

  /**
   * trigger event
   */
  stored: function(){
    pubsub.trigger('boardDirector:render');
  }
});
//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/strengths/form.hbs'); 

Backbone.$ = $;

module.exports = Backbone.View.extend({
  template: "#strength-form-template", 
  
  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save",
  },

  render: function(){
    $(this.el).html(template());
    return this;
  },

  save: function(e){
    var _this = this;
    var $el = $(e.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return _this.model.save(data).done(function(){
      toastr.success('Guardado');
      if (this.type == "POST") {
        _this.stored();
      }
    });
  },

  store: function(e) {
    e.preventDefault();
    this.save(e);
  },

  stored: function(){
    pubsub.trigger('strength:render');
  }
});
//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/executives/form.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  template: "#executive-form-template", 

  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    this.$el.html(template());
    return this;
  },

  /**
   * Get data from form 
   * get transmitter id from localStorage
   * send data to api
   * @param  {[type]} e [description]
   * @return ajax
   */
  save: function(e){
    var _this = this;
    var $el = $(e.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    return this.model.save(data).done(function(){
      //if type of ajax is post execute stored
      if (this.type == "POST") {
        _this.stored();
      }
      
    });
  },

  store: function(e) {
    e.preventDefault();
    this.save(e);
    toastr.success('Guardado');
  },

  stored: function(){
    pubsub.trigger('executive:render');
  }
});
//Dependencies
global.jQuery = require('jquery');
var $ = jQuery;
var Backbone = require('backbone');
var serializeJSON = require('jquery-serializejson');

//Utils
var pubsub = require('../../../utils/pubsub');

//Templates
var template = require('../../templates/investor_relations/form.hbs');

Backbone.$ = $;

module.exports = Backbone.View.extend({
  events: {
    "change .form-control": "store",
    "click .form-checkbox": "save"
  },

  /**
   * append template to $el
   * @return object this
   */
  render: function(){
    this.$el.html(template());
    return this;
  },

  save: function(evt){
    var _this = this;
    var $el = $(evt.currentTarget);
    var data;

    data = $el.parent().parent().serializeJSON();
    data.transmitter_id = localStorage.transmitter_id;

    this.model.save(data).done(function(){
      if (this.type == "POST") {
        _this.stored();
      }
      
    });
  },

  store: function(evt) {
    var _evt = evt;
    _evt.preventDefault();

    this.save(_evt);

    toastr.success('Guardado');
  },

  stored: function(){
   pubsub.trigger('investorRelation:render');
  }
});
//Models
var Transmitter = require('../models/transmitter');
var BoardDirector = require('../models/board_director');
var Strength = require('../models/strength');
var Executive = require('../models/executive');
var InvestorRelation = require('../models/investor_relation');

//Collections
var Transmitters = require('../collections/transmitters');

//Views
var TransmitterCreatePage = require('../views/pages/transmitter/create');
var TransmitterListPage = require('../views/pages/transmitter/list');
var TransmitterList = require('../views/transmitters/list');

var TransmitterCreate = require('../views/transmitters/create');
var TransmitterGraphCreate = require('../views/transmitters/graph_create');
var BoardDirectorCreate = require('../views/board_directors/create');
var StrengthCreate = require('../views/strengths/create');
var ExecutiveCreate = require('../views/executives/create');
var InvestorRelationCreate = require('../views/investor_relations/create');

//Utils
var pubsub = require('../../utils/pubsub');

module.exports = {

  initialize: function(){
    var _this = this;
    var transmitterCreatePage = new TransmitterCreatePage();
    $('body > .container').append(transmitterCreatePage.render().el);

    pubsub.on('boardDirector:render', _this.boardDirectorCreate, _this);
    pubsub.on('strength:render', _this.strengthCreate, _this);
    pubsub.on('executive:render', _this.executiveCreate, _this);
    pubsub.on('investorRelation:render', _this.investorRelationCreate, _this);
  },

  pageList: function(){
    view = new TransmitterListPage();
    $('body > .container').append(view.render().el);
  },

  list: function(){
    var collection = new Transmitters();
    var view = new TransmitterList({collection: collection});
    collection.fetch({reset: true});
    console.log(view);
     $('.transmitters-list').append(view.el);
  },

  create: function(){
    view = new TransmitterCreate( {model: new Transmitter()} );
    $('.transmitter-container').append( view.render().el );
  },
  
  graphCreate: function(){
    view =  new TransmitterGraphCreate( {model: new Transmitter()} );
    $('.transmitter-graphs_container').append( view.render().el );
  },

  boardDirectorCreate: function(){
    var view = new BoardDirectorCreate( {model: new BoardDirector()} );
    $('.board-director-container').append( view.render().el );
  },

  strengthCreate: function(){
    var view = new StrengthCreate( {model: new Strength()} );
    $('.strength-container').append( view.render().el );
  },

  executiveCreate: function(){
    var view = new ExecutiveCreate({model: new Executive()});
    $('.executive-container').append( view.render().el  );
  },

  investorRelationCreate: function(){
    var view = new InvestorRelationCreate( {model: new InvestorRelation()} );
    $('.investor-relation-container').append( view.render().el  );
  }


};
var Backbone = require('backbone');
var $ = require('jquery');
Backbone.$ = $;

var TransmitterController = require('./controllers/transmitter');

module.exports = Backbone.Router.extend({

  routes: {
    '': "transmittersList",
    'admin/transmitters/create': "transmitterCreate",
    'admin/transmitters/:id/edit': "transmitterEdit"
  },

  transmittersList: function(query, page) {
    TransmitterController.pageList();
    TransmitterController.list();
  },

  transmitterCreate: function() {

    TransmitterController.initialize();

    TransmitterController.create();
    TransmitterController.graphCreate();
    TransmitterController.boardDirectorCreate();
    TransmitterController.strengthCreate();
    TransmitterController.executiveCreate();
    TransmitterController.investorRelationCreate();
  },

  transmitterEdit: function(id) {
    
  }

});
