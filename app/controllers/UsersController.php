<?php

class UsersController extends \BaseController {
	
	public function index() {
		return View::make('layouts.login');
	}

	public function login() 
	{
		$password =  Input::get('password');
		$email = Input::get('email');
		$data = ['email' => $email, 'password' =>$password];

		if ( Auth::Attempt($data) ) {
			return Redirect::to('/admin');
		}

		return Redirect::to('/login');
	}
}