<?php

class AdminPagesController extends \BaseController 
{
  protected $layout = "layouts.admin";

  public function index()
  {
  	return View::make('layouts/admin');
  }
  
}