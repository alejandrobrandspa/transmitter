<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function store()
	{
		
		$req = Input::all();
		$validator = Validator::make($req, $this->entity->rules);

		if ($validator->fails()) {
			return Response::json($validator->messages(), 400);
		}

		$model = $this->entity->create($req);
		return Response::json($model, 201);
	}

	public function update($id)
	{
		$req = Input::all();
		$validator = Validator::make($req, $this->entity->rules);

		if ($validator->fails()) {
			return Response::json($validator->messages(), 400);
		}
		
		$model = $this->entity->find($id);
		$model->update($req);
		return Response::json($model, 200);
	}




}
