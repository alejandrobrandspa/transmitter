<?php

class ExecutivesController extends \BaseController {
	protected $entity;

	public function __construct(Executive $entity){
		$this->entity = $entity;
	}

	/**
	 * Display a listing of the resource.
	 * GET /executives
	 *
	 * @return Response
	 */
	public function index()
	{
		$transmitterId = Input::get('transmitter_id');

		//If request send transmitter_id get all by that id
		if (Input::has('transmitter_id')) {
			$collection = $this->entity->where('transmitter_id', $transmitterId)->get();
			return Response::json($collection, 200);
		}
	}



}