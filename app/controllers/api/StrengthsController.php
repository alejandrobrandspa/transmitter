<?php

class StrengthsController extends \BaseController {
	protected $entity;

	public function __construct(Strength $entity){
		$this->entity = $entity;
	}
	/**
	 * Display a listing of the resource.
	 * GET /strengths
	 *
	 * @return Response
	 */
	public function index()
	{
		$transmitterId = Input::get('transmitter_id');

		//If request send transmitter_id get all by that id
		if (Input::has('transmitter_id')) {
			$collection = $this->entity->where('transmitter_id', $transmitterId)->get();
			return Response::json($collection, 200);
		}
	}

}