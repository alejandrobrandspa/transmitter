<?php

class TransmitterController extends \BaseController {
	protected $entity;

	public function __construct(Transmitter $entity){
		$this->entity = $entity;
	}

	/**
	 * Display a listing of the resource.
	 * GET /transmitter
	 *
	 * @return Response
	 */
	public function index()
	{
		$filters = Input::get('filter');
		$sector = Input::get('sector');
		$city = Input::get('city');
		$query = Input::get('query');
		$skip = Input::get('skip');

		$collection = $this->entity->with('boardDirectors', 'executives', 'investorRelations', 'strengths')->orderBy('id', 'DESC')->take(10)->skip($skip);
		
		if ($sector) {
			$collection = $collection->where('sector','=', $sector); 
		}

		if ($city) {
			$collection = $collection->where('city','=', $city); 
		}

		if ($query) {
			$collection = $collection->where('name','LIKE', "%$query%"); 
		}
		
		return Response::json($collection->get() , 200);
		
	}

	/**
	 * Display the specified resource.
	 * GET /transmitter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$model = $this->entity->with('strengths', 'boardDirectors', 'executives', 'investorRelations')->find($id);
		return Response::json($model);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /transmitter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		if (Input::file()){
			foreach ($file as $key => $val) {
				$filename = $this->storeGraph($val);
				$model->$key =  $filename;
				$model->save();
			}
		}

		$model = $this->entity->find($id);
		$model->update(Input::all());
		return Response::json($model);
	
	}

	/**
	 * Upload image graphs to entity
	 * @param  int $id id of entity
	 * @return response
	 */
	public function updateGraphs($id){
		$model = Transmitter::find($id);
		$file = Input::file();
		foreach ($file as $key => $val) {
			$filename = $this->storeGraph($val);
			$model->$key =  $filename;
			$model->save();
		}

		return $filename;
	}

	/**
	 * save photo on uploads 
	 * @param object $file       
	 */
	public function storeGraph($file){	
		$randomString = str_random(12);
		$extension = $file->getClientOriginalExtension();
		$filename = $randomString.".".$extension;
		$path = public_path().'/uploads';
		$file->move($path, $filename);
		return $filename;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /transmitter/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$model = $this->entity->find($id);
		$model->delete();
		return Response::json('', 204);
	}

}