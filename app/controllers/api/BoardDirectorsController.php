<?php

class BoardDirectorsController extends \BaseController {
	protected $entity;

	public function __construct(BoardDirector $entity){
		$this->entity = $entity;
	}

	/**
	 * Display a listing of the resource.
	 * GET /boarddirectors
	 *
	 * @return Response
	 */
	public function index()
	{
		$transmitterId = Input::get('transmitter_id');

		//If request send transmitter_id get all by that id
		if (Input::has('transmitter_id')) {
			$collection = $this->entity->where('transmitter_id', $transmitterId)->get();
			return Response::json($collection, 200);
		}
	}

}