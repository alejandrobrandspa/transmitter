<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Emisores Admin</title>
	<link rel="stylesheet" href="/css/bootstrap.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link rel="stylesheet" href="/node_modules/nprogress/nprogress.css">
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        Emisores Admin
      </a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    	  <ul class="nav navbar-nav">
    	<li><a href="#">Lista emisores</a></li>
    	<li><a href="#transmitters/create">Crear nuevo emisor</a></li>
    </ul>
    </div>
  </div>
  
</nav>
	<div class="container"> </div>

	<script src="/js/dist/dependencies.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

	<script>
		toastr.options = {
			"debug": false,
			"newestOnTop": false,
			"positionClass": "toast-top-center",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "2000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};
	</script>
	
	<script src="/js/dist/app_admin.js"></script>

</body>
</html>