<?php

class Transmitter extends \Eloquent {
	protected $fillable = [
		'name',
		'year_of_incoporation',
		'web_page',
		'sector',
		'city',
		'employees',
		'description_es',
		'description_en',
		'market_capitalization',
		'market_capitalization_closure_date',
		'number_of_shareholders',
		'free_float',
		'dividend',
		'earnings_per_share',
		'ticker_1',
		'ticker_2',
		'badge',
		'type_of_stock_1',
		'type_of_stock_2',
		'isin_1',
		'isin_2',
		'instrument_type_1',
		'instrument_type_2',
		'p_e_1',
		'p_e_2',
		'price_book_1',
		'price_book_2',
		'dividend_yield_1',
		'dividend_yield_2',
		'shares_outstanding_1',
		'shares_outstanding_2',
		'responsability_policy_es',
		'responsability_policy_en',
		'financial_figures_graph',
		'main_shareholders_graph',
		'stock_performance_graph',
		'revenue_segmentation_graph'
	];

	public $rules = [
		'name' => 'required'
	];

	public function boardDirectors()
	{
		return $this->hasMany('BoardDirector');
	}

	public function executives()
	{
		return $this->hasMany('Executive');
	}

	public function strengths()
	{
		return $this->hasMany('Strength');
	}

	public function investorRelations()
	{
		return $this->hasMany('InvestorRelation');
	}
}