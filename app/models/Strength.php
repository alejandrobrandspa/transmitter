<?php

class Strength extends \Eloquent {
	protected $fillable = [
		'transmitter_id',
		'description_es',
		'description_en',
		'position'
	];

	public $rules = [
		'transmitter_id' => 'required',
	];

	public function transmitter()
	{
		return $this->belongsTo('Transmitter');
	}
}