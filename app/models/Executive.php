<?php

class Executive extends \Eloquent {
	protected $fillable = [
		'transmitter_id',
		'name',
		'position_title',
		'position'
	];

	public $rules = [
		'transmitter_id' => 'required',
		'name' => 'required',
	];

	public function transmitter()
	{
		return $this->belongsTo('Transmitter');
	}
}