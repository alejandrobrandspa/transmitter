<?php

class BoardDirector extends \Eloquent {
	protected $fillable = [
		'transmitter_id',
		'name',
		'alternate_member',
		'independent_member',
		'position'
	];

	public $rules = [
		'transmitter_id' => 'required',
		'name' => 'required'
	];

	public function transmitter()
	{
		return $this->belongsTo('Transmitter');
	}
}