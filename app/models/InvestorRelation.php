<?php

class InvestorRelation extends \Eloquent {
	protected $table = "investor_relations";

	protected $fillable = [
		'transmitter_id',
		'name',
		'email',
		'phone',
		'position'
	];

	public $rules = [
		'transmitter_id' => 'required',
		'name' => 'required'
	];

	public function transmitter()
	{
		return $this->belongsTo('Transmitter');
	}
}