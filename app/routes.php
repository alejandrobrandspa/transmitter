<?php

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/login', 'UsersController@index');
Route::post('/login',  'UsersController@login');

Route::group(array('prefix' => 'api'), function()
{
	Route::resource('boarddirectors', 'BoardDirectorsController');
	Route::resource('executives', 'ExecutivesController');
	Route::resource('investorrelations', 'InvestorRelationsController');
	Route::resource('strengths', 'StrengthsController');
	Route::resource('transmitter', 'TransmitterController');
	Route::post('transmitter/{id}/graph', 'TransmitterController@updateGraphs');
	
	// Route::post('transmitter/{id}', 'TransmitterController@update');
	// Route::put('transmitter/{id}', 'TransmitterController@update');
	// Route::post('transmitter', 'TransmitterController@store');
	// Route::get('transmitter', 'TransmitterController@index');
	// Route::get('transmitter/{id}', 'TransmitterController@show');
	// Route::delete('transmitter/{id}', 'TransmitterController@destroy');
});

Route::get('admin', 'AdminPagesController@index');
Route::get('admin/transmitters/{id}/edit', 'AdminPagesController@index');
Route::get('admin', 'AdminPagesController@index');


