<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmittersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transmitters', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('year_of_incoporation');
			$table->string('web_page');
			$table->string('sector');
			$table->string('city');
			$table->integer('employees');
			$table->string('description_es');
			$table->string('description_en');
			$table->string('market_capitalization');
			$table->string('market_capitalization_closure_date');
			$table->string('number_of_shareholders');
			$table->string('total_shares_outstanding');
			$table->string('free_float');
			$table->string('dividend');
			$table->string('earnings_per_share');
			$table->string('note');
			$table->string('badge');
			$table->string('ticker_1');
			$table->string('ticker_2');
			$table->string('type_of_stock_1');
			$table->string('type_of_stock_2');
			$table->string('isin_1');
			$table->string('isin_2');
			$table->string('instrument_type_1');
			$table->string('instrument_type_2');
			$table->string('price_book_1');
			$table->string('price_book_2');
			$table->string('p_e_1');
			$table->string('p_e_2');
			$table->string('dividend_yield_1');
			$table->string('dividend_yield_2');
			$table->string('shares_outstanding_1');
			$table->string('shares_outstanding_2');
			$table->string('responsability_policy_es');
			$table->string('responsability_policy_en');
			$table->string('financial_figures_graph');
			$table->string('main_shareholders_graph');
			$table->string('stock_performance_graph');
			$table->string('revenue_segmentation_graph');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('transmitters');
	}

}
