<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorRelationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investor_relations', function($table)
		{
			$table->increments('id');
			$table->integer('transmitter_id')->unsigned();
			$table->string('name');
			$table->string('email');
			$table->string('phone');
			$table->integer('position');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('investor_relations');
	}

}
