<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardDirectorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('board_directors', function($table)
		{
			$table->increments('id');
			$table->integer('transmitter_id')->unsigned();
			$table->string('name');
			$table->boolean('alternate_member')->default('0');
			$table->boolean('independent_member')->default('0');
			$table->integer('position');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('board_directors');
	}

}
