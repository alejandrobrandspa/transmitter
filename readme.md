# Transmitters app
It's a proyect for the company BVC and works how a directory of transmitters from BVC.

#Dependencies

##Backend
- Laravel 4.2 / Framework MVC PHP with ORM, Authentication, Routing and so on.
- mysql / Relational Database Boring but useful.

##Front End
- Bootstrap / UI
- Jquery / Interact with DOM and ajax.
- Underscore / Functional programming helpers
- Backbone / MV* structure
- Handlebars / Templates
- Broweserify / Modules structure like node
- Gulp / Automate and enhance workflow

### Make it by Brand Spa

#Api

## Resource /transmitter
- POST 

Request type application/json

        {
            "name": "Brand Spa",
            "year_of_incoporation": "1987",
            "web_page": "brandspa.com",
            "sector": "ads",
            "city": "Bogotá",
            "employees": 20,
            "description_es": "Empresa que hace...",
            "description_en": "Company that make",
            "market_capitalization": "",
            "market_capitalization_closure_date": "31 Diciembre 2013",
            "number_of_shareholders": "",
            "free_float": "",
            "dividend": "",
            "earnings_per_share": "",
            "ticker_1": "",
            "ticker_2": "",
            "badge": "",
            "type_of_stock_1": "",
            "type_of_stock_2": "",
            "isin_1": "",
            "isin_2": "",
            "instrument_type_1": "",
            "instrument_type_2": "",
            "p_e_1": "",
            "p_e_2": "",
            "price_book_1": "",
            "price_book_2": "",
            "dividend_yield_1": "",
            "dividend_yield_2": "",
            "shares_outstanding_1": "",
            "shares_outstanding_2": "",
            "responsability_policy_es": "",
            "responsability_policy_en": "",
            "financial_figures_graph": "",
            "main_shareholders_graph": "",
            "stock_performance_graph": "",
            "revenue_segmentation_graph": ""
        }

Request type application/json


- PUT

- GET

- GET  


## Resource /boarddirectors
- POST 

- PUT

- GET

- GET 

## Resource /executives
- POST 

- PUT

- GET

- GET 

## Resource /investorrelations
- POST 

- PUT

- GET

- GET 

## Resource /strengths
- POST 

- PUT

- GET

- GET 






